import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class AuthorController {
    async postAuthorLogin(authorEmail: string, authorPassword: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`) 
            .body({"email": authorEmail, "password": authorPassword})
            .send();
        return response;
    }

    async getAuthorInfo(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author`)
            .bearerToken(accessToken) 
            .send();
        return response;
    }

    async postAuthorInfo(accessToken: string, authorInfo: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`author`)
            .headers({ Authorization: `Bearer ${accessToken}` }) 
            .body(authorInfo)
            .send();
        return response;
    }

    async getUserInfo(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`user/me`)
            .bearerToken(accessToken) 
            .send();
        return response;
    }

    async getAuthorInfoById(userId: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/overview/${userId}`)
            .bearerToken(accessToken) 
            .send();
        return response;
    }
}