import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class authController {
    async authorAuth(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`) 
            //.body({"email": "rob055l5@temporary-mail.net", "password": "aaaaaaaa1"})
            .body({"email": email, "password": password})
            .send();
        return response;
    }
}