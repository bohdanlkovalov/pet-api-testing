import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;


export class articleController {
     async postArticle(accessToken: string, articleInfo: object) {
          const response = await new ApiRequest()
              .prefixUrl(baseUrl)
              .method("POST")
              .url(`article`)
              .headers({ Authorization: `Bearer ${accessToken}` }) 
              .body(articleInfo)
              .send();
          return response;
      }

      async getArticles(accessToken: string) {
          const response = await new ApiRequest()
              .prefixUrl(baseUrl)
              .method("GET")
              .url(`article/author`)
              .bearerToken(accessToken) 
              .send();
          return response;
      }

      async getArticleInfoById(articleId: string, accessToken: string) {
          const response = await new ApiRequest()
              .prefixUrl(baseUrl)
              .method("GET")
              .url(`article/${articleId}`)
              .bearerToken(accessToken) 
              .send();
          return response;
      }

      async postArticleComment(accessToken: string, commentInfo: object) {
          const response = await new ApiRequest()
              .prefixUrl(baseUrl)
              .method("POST")
              .url(`article_comment`)
              .headers({ Authorization: `Bearer ${accessToken}` }) 
              .body(commentInfo)
              .send();
          return response;
      }

      async getArticleComments(articleId: string, accessToken: string) {
          const response = await new ApiRequest()
              .prefixUrl(baseUrl)
              .method("GET")
              .url(`article_comment/of/${articleId}?size=200`)
              .bearerToken(accessToken) 
              .send();
          return response;
      }
}