// import { expect } from "chai";
// import { checkJsonSchema, checkResponseBodyMessage, checkResponseBodyStatus, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
// // const schemas = require('./data/schemas_testData.json');
// // var chai = require('chai');
// // chai.use(require('chai-json-schema'));
// import { authController } from "../lib/controllers/auth.controller";
// const auth = new authController();

// describe("Use test data", () => {
//      let invalidCredentialsDataSet = [
//           {email: "rob055l5@temporary-mail.net", password: "         "},
//           {email: "rob055l5@temporary-mail.net", password: "aaaaaaaa1 "},
//           {email: "rob055l5@temporary-mail.net", password: "aaaaaaaa 1"},
//           {email: "rob055l5@temporary-mail.net", password: "admin"},
//           {email: "rob055l5@temporary-mail.net", password: "rob055l5@temporary-mail.net"},
//           {email: "rob055l5 @temporary-mail.net", password: "aaaaaaaa1"},
//           {email: "rob055l5@temporary-mail.net ", password: "aaaaaaaa1"},
//      ];
//      invalidCredentialsDataSet.forEach((credentials) => {
//           it(`login using invalid credentials: ${credentials.email} + ${credentials.password}`, async () => {
//                let response = await auth.authorAuth(credentials.email, credentials.password);

//                checkStatusCode(response, 401);
//                checkResponseBodyMessage(response, "Bad credentials");
//                checkResponseBodyStatus(response, "UNAUTHORIZED");
//                checkResponseTime(response, 3000);
//           });    
//      });
// })