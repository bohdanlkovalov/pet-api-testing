import { expect } from "chai";
import { checkJsonSchema, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));
import { AuthorController } from "../lib/controllers/author.controller";
import { articleController } from "../lib/controllers/articles.controller";
const author = new AuthorController();
const article = new articleController();


describe("Author controller", () => {
    let authorToken: string;
    let authorId: string;
    let userId: string;
    let articleId: string;

    before(`postAuthorLogin`, async () => {
        let response = await author.postAuthorLogin("rob055l5@temporary-mail.net", "aaaaaaaa1");

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        authorToken = response.body.accessToken;
    });

    it(`getAuthorInfo`, async () => {
        let response = await author.getAuthorInfo(authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkJsonSchema(response, schemas.schema_AuthorInfo);

        authorId = response.body.id;
        userId = response.body.userId;
    });

    it(`postAuthorInfo`, async () => {
        let updateAuthorInfo = {
            "id": authorId,    
            "userId": userId,
            "avatar": null,
            "firstName": 'Rimma',
            "lastName": 'Brown',
            "job": null,
            "location": 'Algeria',
            "company": null,
            "website": '',
            "twitter": 'https://twitter.com/',
            "biography": 'yes'
          };

        let response = await author.postAuthorInfo(authorToken, updateAuthorInfo);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });

    it(`getUserInfo`, async () => {
        let response = await author.getUserInfo(authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkJsonSchema(response, schemas.schema_UserInfo);
    });

    it(`getAuthorInfoById`, async () => {
        let response = await author.getAuthorInfoById(authorId, authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });
    afterEach(function () {
        console.log('It was a test');
    })

    it(`postArticle`, async () => {
        let newArticle = {
            "name": 'New article 10',
            "text": '<p>It is my 10th article.</p>\n',
            "image": 'https://knewless.tk/assets/images/245892e6-19c9-47af-ad15-de65ef5e0003.png',
            "authorId": authorId,
            "authorName": 'Rimma Brown'
          };

        let response = await article.postArticle(authorToken, newArticle);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        articleId = response.body.id;
    });

    it(`Get all articles`, async () => {
        let response = await article.getArticles(authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkJsonSchema(response, schemas.schema_ArticleInfo);

    });

    it(`getArticleInfoById`, async () => {
        let response = await article.getArticleInfoById(articleId, authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

    });

    it(`Post comment`, async () => {
        let newComment = {
            "articleId": articleId,
            "text": 'I love rock n roll and rap and opera'
          };

        let response = await article.postArticleComment(authorToken, newComment);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

    });

    it(`get Article Comments`, async () => {
        let response = await article.getArticleComments(articleId, authorToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

    });
}); 